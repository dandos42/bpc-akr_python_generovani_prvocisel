# Bpc Akr Python Generovani Prvocisel

# Autors:
Daniel Prachař, Dalibor Rada, Matěj Oszelda

# Date of creation 
29.10.2022

# Subject
Kryptografie

# University
BRNO UNIVERSITY OF TECHNOLOGY, FACULTY OF ELECTRICAL ENGINEERING AND INFORMATION TECHNOLOGY


# Project description
Projekt se věnuje problematice generování velkých prvočísel a následné testováních těchto, i jiných, velkých prvočísel pomocí matematických operací, a to za použití různých, minimálně dvou, algoritmů. Zahrnuje teoretický úvod do oblasti prvočísel, jejich hlavní vlastnosti, problém spojený se zpětnou faktorizací a potřebu v moderních kryptografických systémech. Podkapitola generování prvočísel již konkrétně míří na algoritmy, které se u generování mohou využívat, a které byly nakonec využity. S tím je spjatá podkapitola s testováním prvočíselnosti, konkrétně druhy testů prvočíselnosti a jejich popis. Projekt dále zakomponovává možnost výpisu všech prvočísel po zadané číslo uživatelem za účelem zjištění prvočísel. Využívá práci se soubory, přesněji pak využití dat jako vstup ze souboru pro test prvočíselnosti, popř. export prvočísla do jiného samostatného souboru pro možné další využití. Nakonec je aplikace schopná zaznamenávat probíhající události do logu, a to včetně výpisu doby trvání generování prvočísel a i jejich testování. Poslední nejobsáhlejší kapitola obsahuje samotnou studii doposud rozpracovaného projektu v programovacím jazyce Python a další kroky potřebné k úspěšnému dokončení projektu.


# File description
Main.py – Hlavní soubor obsahující kód 

Random_cislo.txt – Do tohoto souboru je možné vložit nějaké náhodné číslo. Program následně otestuje jestli je číslo prvočíslem

Seznam_prvočisel.txt – V tomto souboru nalezneme seznam prvočísel, které program vypsal po námi zadané číslo

Log.log – Tento soubor obsahuje logy s informacemi o průběhu programu

dokumentace.docx - Základní dokumentace k našemu programu

studie.docx - studie problematiky

10.docx 
# Licence
Pro školní účely

